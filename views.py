from flask import jsonify, redirect
from app import app
from app import db
import models

@app.route("/")
def index():
    return redirect("/static/index.html")


