from app import db, app


@app.cli.command()
def initdb():
   db.create_all()

